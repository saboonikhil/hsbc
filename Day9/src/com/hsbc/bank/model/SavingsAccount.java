package com.hsbc.bank.model;

import java.io.Serializable;

public class SavingsAccount implements Comparable<SavingsAccount>, Serializable {

	private static final long serialVersionUID = 1L;
	private String customerName;
	private long accountNumber;
	private double accountBalance;
	private final String emailAddress;

	public SavingsAccount(String customerName, double accountBalance, String emailAddress) {
		this.customerName = customerName;
		this.accountBalance = accountBalance;
		this.emailAddress = emailAddress;
	}

	public SavingsAccount(long accountNumber, String customerName, double accountBalance, String emailAddress) {
		this.customerName = customerName;
		this.accountBalance = accountBalance;
		this.emailAddress = emailAddress;
		this.accountNumber = accountNumber;
	}

	public String getCustomerName() {
		return this.customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public double getAccountBalance() {
		return this.accountBalance;
	}

	public void setAccountBalance(double accountBalance) {
		this.accountBalance = accountBalance;
	}

	public long getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(long accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	@Override
	public int compareTo(SavingsAccount account) {
		return -1 * account.customerName.compareTo(this.customerName);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (accountNumber ^ (accountNumber >>> 32));
		result = prime * result + ((customerName == null) ? 0 : customerName.hashCode());
		result = prime * result + ((emailAddress == null) ? 0 : emailAddress.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof SavingsAccount))
			return false;
		SavingsAccount other = (SavingsAccount) obj;
		if (accountNumber != other.accountNumber)
			return false;
		if (customerName == null) {
			if (other.customerName != null)
				return false;
		} else if (!customerName.equals(other.customerName))
			return false;
		if (emailAddress == null) {
			if (other.emailAddress != null)
				return false;
		} else if (!emailAddress.equals(other.emailAddress))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "SavingsAccount [customerName=" + customerName + ", accountNumber=" + accountNumber + ", accountBalance="
				+ accountBalance + ", emailAddress=" + emailAddress + "]";
	}
}