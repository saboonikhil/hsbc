package com.hsbc.bank.client;

import java.util.Random;

import com.hsbc.bank.controller.SavingsAccountController;
import com.hsbc.bank.dao.SavingsAccountDAO;
import com.hsbc.bank.model.SavingsAccount;
import com.hsbc.bank.service.SavingsAccountService;
import com.hsbc.bank.util.SavingsAccountDAOFactory;
import com.hsbc.bank.util.SavingsAccountServiceFactory;

public class SavingsAccountClient {

	public static void main(String[] args) {

		SavingsAccountDAO dao = SavingsAccountDAOFactory.getSavingsAccountDAO();
		SavingsAccountService service = SavingsAccountServiceFactory.getInstance(dao);
		SavingsAccountController controller = new SavingsAccountController(service);

		SavingsAccount kiranSavingsAccount = controller.openSavingsAccount("hari", 25_000,
				getSaltString() + "@gmail.com");

		System.out.println("Account Id: " + kiranSavingsAccount.getAccountNumber());
	}

	public static String getSaltString() {
		String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
		StringBuilder salt = new StringBuilder();
		Random rnd = new Random();
		while (salt.length() < 10) { // length of the random string.
			int index = (int) (rnd.nextFloat() * SALTCHARS.length());
			salt.append(SALTCHARS.charAt(index));
		}
		String saltStr = salt.toString();
		return saltStr;

	}

}
