package com.hsbc.bank.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import com.hsbc.bank.exception.ClientNotFoundException;
import com.hsbc.bank.model.SavingsAccount;

public class JDBCBackedSavingsAccountDAOImpl implements SavingsAccountDAO {

	private static String connectString = "jdbc:derby://localhost:1527/mydb ";
	private static String username = "app";
	private static String password = "password";

	private static final String INSERT_QUERY = "insert into savings_account (account_number, cust_name, account_balance)"
			+ " values ";
	private static final String INSERT_QUERY_PREP_STMT = "insert into savings_account (cust_name, account_balance, email_address)"
			+ " values ( ?, ?, ?)";
	private static final String SELECT_QUERY = "select * from savings_account";
	private static final String DELETE_BY_ID_QUERY = "delete from savings_account where account_number=";
	private static final String SELECT_BY_ID_QUERY = "select *  from savings_account where account_number=";
	private static final String SELECT_BY_EMAIL_QUERY = "select *  from savings_account where email_address= ?";

	private static Statement getStatement() {
		try {
			Connection connection = DriverManager.getConnection(connectString, username, password);
			// System.out.println("Database successfully connected");
			return connection.createStatement();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	private static Connection getDBConnection() {
		try {
			Connection connection = DriverManager.getConnection(connectString, username, password);
			return connection;

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public SavingsAccount saveSavingsAccount(SavingsAccount savingsAccount) {
		int numberOfRecordsUpdated = 0;
		try (PreparedStatement pStmt = getDBConnection().prepareStatement(INSERT_QUERY_PREP_STMT);) {
			pStmt.setString(1, savingsAccount.getCustomerName());
			pStmt.setDouble(2, savingsAccount.getAccountBalance());
			pStmt.setString(3, savingsAccount.getEmailAddress());

			numberOfRecordsUpdated = pStmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		if (numberOfRecordsUpdated == 1) {
			try {
				SavingsAccount fetchedSavingsAccount = fetchSavingsAccountByEmailAddress(
						savingsAccount.getEmailAddress());
				if (fetchedSavingsAccount != null) {
					return fetchedSavingsAccount;
				}

			} catch (ClientNotFoundException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	@Override
	public Collection<SavingsAccount> fetchSavingsAccounts() {
		String query = SELECT_QUERY;
		Set<SavingsAccount> savingsAccountSet = new HashSet<>();
		try {
			ResultSet rs = getStatement().executeQuery(query);
			if (rs.next()) {
				SavingsAccount savingsAccount = new SavingsAccount(rs.getInt("account_number"),
						rs.getString("cust_name"), rs.getDouble("account_balance"), rs.getString("email_address"));
				savingsAccountSet.add(savingsAccount);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return savingsAccountSet;
	}

	@Override
	public SavingsAccount fetchSavingsAccountByAccountId(long accountNumber) throws ClientNotFoundException {
		String query = SELECT_BY_ID_QUERY + " " + accountNumber;
		try {
			ResultSet rs = getStatement().executeQuery(query);
			while (rs.next()) {
				SavingsAccount savingsAccount = new SavingsAccount(rs.getInt("account_number"),
						rs.getString("cust_name"), rs.getDouble("account_balance"), rs.getString("email_address"));
				return savingsAccount;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new ClientNotFoundException(" Customer with " + accountNumber + " does not exists");
		}
		return null;
	}

	@Override
	public SavingsAccount fetchSavingsAccountByEmailAddress(String emailAddress) throws ClientNotFoundException {

		try {
			Connection connection = getDBConnection();
			PreparedStatement ps = connection.prepareStatement(SELECT_BY_EMAIL_QUERY);
			ps.setString(1, emailAddress);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				// System.out.println("Account number: " + rs.getInt("account_number"));
				SavingsAccount savingsAccount = new SavingsAccount(rs.getInt("account_number"),
						rs.getString("cust_name"), rs.getDouble("account_balance"), rs.getString("email_address"));
				// System.out.println("Before returning the savings account back to the customer
				// ");
				System.out.println(savingsAccount);
				return savingsAccount;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new ClientNotFoundException(" Customer with " + emailAddress + " does not exists");
		}
		return null;
	}

	@Override
	public SavingsAccount updateSavingsAccount(long accountNumber, SavingsAccount savingsAccount) {
		// TODO
		return null;
	}

	@Override
	public void deleteSavingsAccount(long accountNumber) {
		String query = DELETE_BY_ID_QUERY + accountNumber;
		try {
			getStatement().executeUpdate(query);

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
