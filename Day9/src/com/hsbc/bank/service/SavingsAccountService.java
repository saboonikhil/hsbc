package com.hsbc.bank.service;

import java.util.Collection;

import com.hsbc.bank.exception.ClientNotFoundException;
import com.hsbc.bank.model.SavingsAccount;

public interface SavingsAccountService {
	
	public SavingsAccount createSavingsAccount(String customerName, double accountBalance, String emailAddress);

	public void deleteSavingsAccount(long accountNumber);

	public Collection<SavingsAccount> fetchSavingsAccounts();

	public SavingsAccount fetchAccountByPIN(int pin);

	public SavingsAccount fetchSavingsAccountByAccountId(long accountNumber) throws ClientNotFoundException;

	public double withdraw(long accountId, double amount) throws ClientNotFoundException;

	public double deposit(long accountId, double amount) throws ClientNotFoundException;

	public double checkBalance(long accountId) throws ClientNotFoundException;

	public void transfer(long accountId, long toId, double amount) throws ClientNotFoundException;
}