package com.hsbc.bank.service;

import java.util.Collection;

import com.hsbc.bank.dao.SavingsAccountDAO;
import com.hsbc.bank.exception.ClientNotFoundException;
import com.hsbc.bank.model.SavingsAccount;

public class SavingsAccountServiceImpl implements SavingsAccountService {

	private SavingsAccountDAO dao;

	public SavingsAccountServiceImpl(SavingsAccountDAO dao) {
		this.dao = dao;
	}

	public SavingsAccount createSavingsAccount(String customerName, double accountBalance, String emailAddress) {
		SavingsAccount savingsAccount = new SavingsAccount(customerName, accountBalance, emailAddress);
		SavingsAccount savingsAccountCreated = this.dao.saveSavingsAccount(savingsAccount);
		return savingsAccountCreated;
	}

	public void deleteSavingsAccount(long accountNumber) {
		this.dao.deleteSavingsAccount(accountNumber);
	}

	public Collection<SavingsAccount> fetchSavingsAccounts() {
		Collection<SavingsAccount> accounts = this.dao.fetchSavingsAccounts();
		return accounts;
	}

	public SavingsAccount fetchAccountByPIN(int pin) {
		return null;
	}

	public SavingsAccount fetchSavingsAccountByAccountId(long accountNumber) throws ClientNotFoundException {
		SavingsAccount savingsAccount = this.dao.fetchSavingsAccountByAccountId(accountNumber);
		return savingsAccount;
	}

	public double withdraw(long accountId, double amount) throws ClientNotFoundException {
		SavingsAccount savingsAccount = this.dao.fetchSavingsAccountByAccountId(accountId);
		if (savingsAccount != null) {
			double currentAccountBalance = savingsAccount.getAccountBalance();
			if (currentAccountBalance >= amount) {
				currentAccountBalance = currentAccountBalance - amount;
				savingsAccount.setAccountBalance(currentAccountBalance);
				this.dao.updateSavingsAccount(accountId, savingsAccount);
				return amount;
			}
		}
		return 0;
	}

	public double deposit(long accountId, double amount) throws ClientNotFoundException {
		SavingsAccount savingsAccount = this.dao.fetchSavingsAccountByAccountId(accountId);
		if (savingsAccount != null) {
			double currentAccountBalance = savingsAccount.getAccountBalance();
			savingsAccount.setAccountBalance(currentAccountBalance + amount);
			this.dao.updateSavingsAccount(accountId, savingsAccount);
			return savingsAccount.getAccountBalance();
		}
		return 0;
	}

	public double checkBalance(long accountId) throws ClientNotFoundException {
		SavingsAccount savingsAccount = this.dao.fetchSavingsAccountByAccountId(accountId);
		if (savingsAccount != null) {
			return savingsAccount.getAccountBalance();
		}
		return 0;
	}

	public void transfer(long accountId, long toId, double amount) throws ClientNotFoundException {
		SavingsAccount fromAccount = this.dao.fetchSavingsAccountByAccountId(accountId);
		SavingsAccount toAccount = this.dao.fetchSavingsAccountByAccountId(toId);
		double updatedBalance = this.withdraw(fromAccount.getAccountNumber(), amount);
		if (updatedBalance != 0) {
			this.deposit(toAccount.getAccountNumber(), amount);
		}
	}

}
