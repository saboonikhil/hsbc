package com.hsbc.bank.util;

import com.hsbc.bank.dao.JDBCBackedSavingsAccountDAOImpl;
import com.hsbc.bank.dao.SavingsAccountDAO;

public class SavingsAccountDAOFactory {

	public static SavingsAccountDAO getSavingsAccountDAO() {
		return new JDBCBackedSavingsAccountDAOImpl();
	}

}
