package com.hsbc.da1.date;

import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

public class DateConversion {

	public static void main(String[] args) {

		System.out.print("Please enter date of birth in dd-MM-yyyy format: ");
		Scanner sc = new Scanner(System.in);
		String input = sc.nextLine();
		sc.close();

		LocalDate parsedDate = LocalDate.parse(input, DateTimeFormatter.ofPattern("dd-MM-yyyy"));
		LocalDate now = LocalDate.now();

		Period diff = Period.between(parsedDate, now);

		System.out.println();
		System.out.println("Number of days till current date: " + (diff.getYears() * 365) + (12 * diff.getMonths())
				+ diff.getDays());
		System.out.println("Day of the week on " + parsedDate + ": " + parsedDate.getDayOfWeek());

		LocalDate futureDate = now.plusYears(4);
		System.out.println("Day of the week after 4 years from " + now + ": " + futureDate.getDayOfWeek());
	}

}
