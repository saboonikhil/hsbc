package com.hsbc.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class AgeCalculatorServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		String ageString = req.getParameter("dob");
		LocalDate dob = LocalDate.parse(ageString, DateTimeFormatter.ofPattern("dd-MM-yyyy"));

		LocalDate now = LocalDate.now();
		Period difference = Period.between(dob, now);
		PrintWriter pw = resp.getWriter();

		pw.write("<h1> You are " + difference.getDays() + " years, " + difference.getMonths() + " month and "
				+ difference.getYears() + " days old.</h1>");
		// System.out.println();

	}

}
