package com.hsbc.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class GreetingServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		String firstName = req.getParameter("firstName");
		String lastName = req.getParameter("lastName");

		LocalDateTime currentDate = LocalDateTime.now();

		PrintWriter out = resp.getWriter();

		out.write("<h1>Welcome " + firstName + " " + lastName + "! The current Date is: " + currentDate.toString()
				+ " </h1>");

	}

}
