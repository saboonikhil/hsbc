public class CurrentAccountClient {

  public static void main(String[] args) {
      CurrentAccount[] accounts = CurrentAccountRegister.getAllAccounts();
      CurrentAccount rajeesh = accounts[0];
      System.out.println("Customer Name: " + rajeesh.getCustomerName());
      System.out.println("Account Number " + rajeesh.getAccountNumber());
      System.out.println("Initial Account balance " + rajeesh.checkBalance());
      System.out.println("Account Balance " + rajeesh.deposit(5000));
      System.out.println("Balance after deposit" + rajeesh.checkBalance());
      rajeesh.withdraw(2000);
      System.out.println("Balance after Withdraw " + rajeesh.checkBalance());

      System.out.println(" ---------------------------------");

      CurrentAccount naveen = accounts[1];
      System.out.println("Account Number " + naveen.getAccountNumber());
      System.out.println("Customer Name: " + naveen.getCustomerName());
      System.out.println("Initial Account balance " + naveen.checkBalance());
      naveen.withdraw(40000);
      System.out.println("Balance after Withdraw " + naveen.checkBalance());
      naveen.transferAmount(20000, rajeesh.getAccountNumber());
      System.out.println("Rajeesh balance: " + rajeesh.checkBalance());
      System.out.println("Naveen balance: " + naveen.checkBalance());

      System.out.println(" ---------------------------------");

      CurrentAccount vinay = accounts[2];
      System.out.println("Customer Name: " + vinay.getCustomerName());
  }
}