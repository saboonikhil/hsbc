public class CallByValRef {

    public static void main(String[] args) {

        int array[] = new int[] { 11, 22, 33, 44 };

        int operand1 = 20;
        int operand2 = 30;

        System.out.printf("Changes before the call");
        for (int a : array) {
            System.out.println(a);
        }
        System.out.println("-------------------------------");
        callByRef(array);

        System.out.printf("Changes before the call");
        for (int a : array) {
            System.out.println(a);
        }
    }

    private static void callByValue(int operand1, int operand2) {
        operand1 = operand1 * 40;
        operand2 = operand2 * 40;

        System.out.printf("Local changes applied  %d %d \n", operand1, operand2);

    }

    private static void callByRef(int[] arr) {
        System.out.println("-------------------------------");
        System.out.print("Changes inside the method");

        arr[0] = 44;
        arr[1] = 55;
        arr[2] = 66;
        arr[3] = 77;
        for (int a : arr) {
            System.out.println(a);
        }

    }
}