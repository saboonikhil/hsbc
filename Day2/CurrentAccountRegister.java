public class CurrentAccountRegister {
    private static CurrentAccount[] currentAccounts = new CurrentAccount[] {
            new CurrentAccount("Rajeesh", "AHHDJAD72893", "HSBC Technology India"),
            new CurrentAccount("Naveen", 80000, "KSKHHSF8484", "InfoTech Solutions LLP"),
            new CurrentAccount("Vinay", new Address("8th Ave", "Bangalore", "Karnataka", 577142), "TWYHSF8484")
    };

    public static CurrentAccount fetchCurrentAccountByAccountId(long accountId) {
        for (CurrentAccount currentAccount : currentAccounts) {
            if (currentAccount.getAccountNumber() == accountId) {
                return currentAccount;
            }
        }
        return null;
    }

    public static CurrentAccount[] getAllAccounts(){
      return currentAccounts;
    }
}