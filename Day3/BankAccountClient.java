abstract class BankAccount {

  final private long accountId;
  private String accountName;

  public abstract void deposit(double amount);

  public abstract void withdraw(double amount);

  public abstract boolean minimumBalValidation(double amount);

  public abstract double getBalance();

  public abstract boolean isEligibleForLoan(double amount);

  public BankAccount(long accountId, String accountName) {
    this.accountId = accountId;
    this.accountName = accountName;
  }
}

final class CurrentAccount extends BankAccount {
  private double accountBalance;
  private String gstNumber;

  public CurrentAccount(long accountId, String accountName, String gstNumber) {
    super(accountId, accountName);
    this.gstNumber = gstNumber;
  }

  final public void withdraw(double amount) {
    if (minimumBalValidation(amount)) {
      System.out.println("Successfully withdrawn " + amount);
      this.accountBalance = this.accountBalance - amount;
    } else {
      System.out.println("Cannot withdraw " + amount);
    }
  }

  final public void deposit(double amount) {
    System.out.println("Successfully deposited " + amount);
    this.accountBalance = accountBalance + amount;
  }

  final public boolean minimumBalValidation(double amount) {
    if ((this.accountBalance - 25000) >= amount) {
      return true;
    }
    return false;
  }

  final public double getBalance() {
    return this.accountBalance;
  }

  final public boolean isEligibleForLoan(double amount) {
    if (amount <= 2500000) {
      System.out.println("You are eligible for loan of " + amount);
      return true;
    }
    System.out.println("You are not eligible for loan of " + amount);
    return false;
  }
}

final class SalariedAccount extends BankAccount {
  private double accountBalance;

  public SalariedAccount(long accountId, String accountName) {
    super(accountId, accountName);
  }

  final public void withdraw(double amount) {
    if (minimumBalValidation(amount) && amount <= 15000) {
      System.out.println("Successfully withdrawn " + amount);
      this.accountBalance = this.accountBalance - amount;
    } else {
      System.out.println("Cannot withdraw " + amount);
    }
  }

  final public void deposit(double amount) {
    System.out.println("Successfully deposited " + amount);
    this.accountBalance = accountBalance + amount;
  }

  final public boolean minimumBalValidation(double amount) {
    if ((this.accountBalance - 0) >= amount) {
      return true;
    }
    return false;
  }

  final public double getBalance() {
    return this.accountBalance;
  }

  final public boolean isEligibleForLoan(double amount) {
    if (amount <= 1000000) {
      System.out.println("You are eligible for loan of " + amount);
      return true;
    }
    System.out.println("You are not eligible for loan of " + amount);
    return false;
  }
}

final class SavingsAccount extends BankAccount {
  private double accountBalance;

  public SavingsAccount(long accountId, String accountName) {
    super(accountId, accountName);
  }

  final public void withdraw(double amount) {
    if (minimumBalValidation(amount) && amount <= 10000) {
      System.out.println("Successfully withdrawn " + amount);
      this.accountBalance = this.accountBalance - amount;
    } else {
      System.out.println("Cannot withdraw " + amount);
    }
  }

  final public void deposit(double amount) {
    System.out.println("Successfully deposited " + amount);
    this.accountBalance = accountBalance + amount;
  }

  final public boolean minimumBalValidation(double amount) {
    if ((this.accountBalance - 10000) >= amount) {
      return true;
    }
    return false;
  }

  final public double getBalance() {
    return this.accountBalance;
  }

  final public boolean isEligibleForLoan(double amount) {
    if (amount <= 500000) {
      System.out.println("You are eligible for loan of " + amount);
      return true;
    }
    System.out.println("You are not eligible for loan of " + amount);
    return false;
  }
}

public class BankAccountClient {
  public static void main(String[] args) {
    int value = Integer.parseInt(args[0]);

    BankAccount ba = null;
    if (value == 1) {
      ba = new CurrentAccount(8382738237L, "Vinay", "HSUAHU8288");
      ba.deposit(60000);
      System.out.println("Your Balance: " + ba.getBalance());
      ba.withdraw(50000);
      System.out.println("Your Balance: " + ba.getBalance());
    } else if (value == 2) {
      ba = new SavingsAccount(7297993797L, "Sanjay");
      ba.deposit(20000);
      System.out.println("Your Balance: " + ba.getBalance());
      ba.withdraw(20000);
      System.out.println("Your Balance: " + ba.getBalance());
    } else {
      ba = new SalariedAccount(9497929920L, "Vijay");
      ba.deposit(30000);
      System.out.println("Your Balance: " + ba.getBalance());
      ba.withdraw(5000);
      System.out.println("Your Balance: " + ba.getBalance());
    }
  }
}
