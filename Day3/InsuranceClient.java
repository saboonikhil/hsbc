interface Insurance {
  double calculatePremium(String vehicleName, double insuredAmount, String model);

  String payPremium(double premiumAmount, String vehicleNumber, String model);
}

class BajajInsurance implements Insurance {
  private static int policyNumberTracker = 100;

  public double calculatePremium(String vehicleName, double insuredAmount, String model) {
    double premium = 0.035 * insuredAmount;
    return premium;
  }

  public String payPremium(double premiumAmount, String vehicleNumber, String model) {
    String policyNumber = "BJ" + vehicleNumber + model + (++policyNumberTracker);
    return policyNumber;
  }
}

class TataAIGInsurance implements Insurance {
  private static int policyNumberTracker = 100;

  public double calculatePremium(String vehicleName, double insuredAmount, String model) {
    double premium = 0.05 * insuredAmount;
    return premium;
  }

  public String payPremium(double premiumAmount, String vehicleNumber, String model) {
    String policyNumber = "TA" + vehicleNumber + model + (++policyNumberTracker);
    return policyNumber;
  }
}

public class InsuranceClient {
  public static void main(String[] args) {

    Insurance insurance = null;

    String input = args[0];
    switch (input) {
      case "1":
        BajajInsurance bajajIn = new BajajInsurance();
        insurance = bajajIn;
        break;
      case "2":
        TataAIGInsurance tataIn = new TataAIGInsurance();
        insurance = tataIn;
        break;
      default:
        System.out.println("Please select either 1 or 2");
        System.exit(0);
        break;
    }

    double premium = insurance.calculatePremium("Bajaj Auto", 250000, "KING");
    System.out.println("Your insurance premium is " + premium);
    System.out.println("Your policy number is " + insurance.payPremium(premium, "TN92H2930", "KING"));
  }
}