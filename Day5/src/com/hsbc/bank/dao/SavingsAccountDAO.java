package com.hsbc.bank.dao;

import java.util.List;

import com.hsbc.bank.exception.ClientNotFoundException;
import com.hsbc.bank.model.SavingsAccount;

public interface SavingsAccountDAO {
	
	
	SavingsAccount saveSavingsAccount(SavingsAccount savingsAccount);
	
	SavingsAccount updateSavingsAccount(long accountNumber, SavingsAccount savingsAccount);
	
	void deleteSavingsAccount(long accountNumber) throws ClientNotFoundException;
	
	List<SavingsAccount> fetchSavingsAccounts();
	
	SavingsAccount fetchSavingsAccountByAccountId(long accountNumber) throws ClientNotFoundException;

}