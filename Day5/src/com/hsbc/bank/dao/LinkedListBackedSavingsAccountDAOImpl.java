package com.hsbc.bank.dao;

import java.util.LinkedList;
import java.util.List;

import com.hsbc.bank.exception.ClientNotFoundException;
import com.hsbc.bank.model.SavingsAccount;

public class LinkedListBackedSavingsAccountDAOImpl implements SavingsAccountDAO {

	private static List<SavingsAccount> savingsAccounts = new LinkedList<>();

	public SavingsAccount saveSavingsAccount(SavingsAccount savingsAccount) {
		savingsAccounts.add(savingsAccount);
		return savingsAccount;
	}

	public SavingsAccount updateSavingsAccount(long accountNumber, SavingsAccount savingsAccount) {
		for (int index = 0; index < savingsAccounts.size(); index++) {
			if (savingsAccounts.get(index).getAccountNumber() == accountNumber) {
				savingsAccounts.set(index, savingsAccount);
				break;
			}
		}
		return savingsAccount;
	}

	public void deleteSavingsAccount(long accountNumber) throws ClientNotFoundException {
		for (int index = 0; index < savingsAccounts.size(); index++) {
			if (savingsAccounts.get(index).getAccountNumber() == accountNumber) {
				savingsAccounts.remove(index);
				break;
			}
		}
		throw new ClientNotFoundException("Please enter a valid account number");
	}

	public List<SavingsAccount> fetchSavingsAccounts() {
		return savingsAccounts;
	}

	public SavingsAccount fetchSavingsAccountByAccountId(long accountNumber) throws ClientNotFoundException {
		for (int index = 0; index < savingsAccounts.size(); index++) {
			if (savingsAccounts.get(index).getAccountNumber() == accountNumber) {
				return savingsAccounts.get(index);
			}
		}
		throw new ClientNotFoundException("Please enter a valid account number");
	}
}
