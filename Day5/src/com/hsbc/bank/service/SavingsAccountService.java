package com.hsbc.bank.service;

import java.util.List;

import com.hsbc.bank.exception.ClientNotFoundException;
import com.hsbc.bank.model.SavingsAccount;

public interface SavingsAccountService {

	public SavingsAccount createSavingsAccount(String customerName, double accountBalance);

	public SavingsAccount createSavingsAccount(String customerName, double accountBalance, String street, String city,
			String state, int zipCode);

	public void deleteSavingsAccount(long accountNumber) throws ClientNotFoundException;

	public List<SavingsAccount> fetchSavingsAccounts();

	public SavingsAccount fetchAccountByPIN(int pin);

	public SavingsAccount fetchSavingsAccountByAccountId(long accountNumber) throws ClientNotFoundException;

	public double withdraw(long accountId, double amount) throws ClientNotFoundException;

	public double deposit(long accountId, double amount) throws ClientNotFoundException;

	public double checkBalance(long accountId) throws ClientNotFoundException;

	public void transfer(long accountId, long toId, double amount) throws ClientNotFoundException;
}