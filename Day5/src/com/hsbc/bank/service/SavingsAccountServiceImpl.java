package com.hsbc.bank.service;

import java.util.List;

import com.hsbc.bank.dao.SavingsAccountDAO;
import com.hsbc.bank.exception.ClientNotFoundException;
import com.hsbc.bank.model.Address;
import com.hsbc.bank.model.SavingsAccount;
import com.hsbc.bank.util.SavingsAccountDAOFactory;

public class SavingsAccountServiceImpl implements SavingsAccountService {

	private SavingsAccountDAO dao = SavingsAccountDAOFactory.getSavingsAccountDAO();

	public SavingsAccount createSavingsAccount(String customerName, double accountBalance) {
		SavingsAccount savingsAccount = new SavingsAccount(customerName, accountBalance);
		SavingsAccount savingsAccountCreated = this.dao.saveSavingsAccount(savingsAccount);
		return savingsAccountCreated;
	}

	public SavingsAccount createSavingsAccount(String customerName, double accountBalance, String street, String city,
			String state, int zipCode) {
		Address address = new Address(street, city, state, zipCode);
		SavingsAccount savingsAccount = new SavingsAccount(customerName, accountBalance, address);
		SavingsAccount savingsAccountCreated = this.dao.saveSavingsAccount(savingsAccount);
		return savingsAccountCreated;
	}

	public void deleteSavingsAccount(long accountNumber) throws ClientNotFoundException {
		this.dao.deleteSavingsAccount(accountNumber);
	}

	public List<SavingsAccount> fetchSavingsAccounts() {
		List<SavingsAccount> accounts = this.dao.fetchSavingsAccounts();
		return accounts;
	}

	public SavingsAccount fetchAccountByPIN(int pin) {
		return null;
	}

	public SavingsAccount fetchSavingsAccountByAccountId(long accountNumber) throws ClientNotFoundException {
		SavingsAccount savingsAccount = this.dao.fetchSavingsAccountByAccountId(accountNumber);
		return savingsAccount;
	}

	public double withdraw(long accountId, double amount) throws ClientNotFoundException {
		SavingsAccount savingsAccount = this.dao.fetchSavingsAccountByAccountId(accountId);
		if (savingsAccount != null) {
			double currentAccountBalance = savingsAccount.getAccountBalance();
			if (currentAccountBalance >= amount) {
				currentAccountBalance = currentAccountBalance - amount;
				savingsAccount.setAccountBalance(currentAccountBalance);
				this.dao.updateSavingsAccount(accountId, savingsAccount);
				return amount;
			}
		}
		return 0;
	}

	public double deposit(long accountId, double amount) throws ClientNotFoundException {
		SavingsAccount savingsAccount = this.dao.fetchSavingsAccountByAccountId(accountId);
		if (savingsAccount != null) {
			double currentAccountBalance = savingsAccount.getAccountBalance();
			savingsAccount.setAccountBalance(currentAccountBalance + amount);
			this.dao.updateSavingsAccount(accountId, savingsAccount);
			return savingsAccount.getAccountBalance();
		}
		return 0;
	}

	public double checkBalance(long accountId) throws ClientNotFoundException {
		SavingsAccount savingsAccount = this.dao.fetchSavingsAccountByAccountId(accountId);
		if (savingsAccount != null) {
			return savingsAccount.getAccountBalance();
		}
		return 0;
	}

	public void transfer(long accountId, long toId, double amount) throws ClientNotFoundException {
		SavingsAccount fromAccount = this.dao.fetchSavingsAccountByAccountId(accountId);
		SavingsAccount toAccount = this.dao.fetchSavingsAccountByAccountId(toId);
		double updatedBalance = this.withdraw(fromAccount.getAccountNumber(), amount);
		if (updatedBalance != 0) {
			this.deposit(toAccount.getAccountNumber(), amount);
		}
	}

}
