package com.hsbc.bank.model;

public class SavingsAccount {

	private String customerName;
	private long accountNumber;
	private double accountBalance;
	private static long counter = 1000;
	private Address address;

	public SavingsAccount(String customerName, double accountBalance) {
		this.customerName = customerName;
		this.accountBalance = accountBalance;
		this.accountNumber = ++counter;
	}

	public SavingsAccount(String customerName, double accountBalance, Address address) {
		this.customerName = customerName;
		this.accountBalance = accountBalance;
		this.accountNumber = ++counter;
		this.address = address;
	}

	public String getCustomerName() {
		return this.customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public double getAccountBalance() {
		return this.accountBalance;
	}

	public void setAccountBalance(double accountBalance) {
		this.accountBalance = accountBalance;
	}

	public long getAccountNumber() {
		return accountNumber;
	}

	public Address getAddress() {
		return address;
	}
}