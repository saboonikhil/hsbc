package com.hsbc.bank.client;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.hsbc.bank.controller.SavingsAccountController;
import com.hsbc.bank.exception.ClientNotFoundException;
import com.hsbc.bank.model.SavingsAccount;

public class SavingsAccountClient {

	public static void main(String[] args) {

		Logger LOGGER = Logger.getLogger(SavingsAccountClient.class.getName());
		SavingsAccountController controller = new SavingsAccountController();

		// initializing new accounts
		SavingsAccount account1 = controller.openSavingsAccount("Rakesh", 50000);
		SavingsAccount account2 = controller.openSavingsAccount("Rajesh", 20000);
		SavingsAccount account3 = controller.openSavingsAccount("Ramesh", 10000, "N.S. Road", "Kolkata", "WB", 700001);

		// transferring 20000 from account1 to account2
		try {
			controller.transfer(18387173L, account2.getAccountNumber(), 20000);
		} catch (ClientNotFoundException e) {
			System.out.println(e.getMessage());
			LOGGER.log(Level.INFO, e.getMessage());
		}

		// transferring 10000 from account2 to account3
		try {
			controller.transfer(account2.getAccountNumber(), account3.getAccountNumber(), 10000);
		} catch (ClientNotFoundException e) {
			System.out.println(e.getMessage());
			LOGGER.log(Level.INFO, e.getMessage());
		}

		// depositing 10000 to account1
		try {
			controller.deposit(account1.getAccountNumber(), 10000);
		} catch (ClientNotFoundException e) {
			System.out.println(e.getMessage());
			LOGGER.log(Level.INFO, e.getMessage());
		}

		// fetching all accounts stored in the database
		List<SavingsAccount> savingsAccounts = controller.fetchSavingsAccounts();

		for (SavingsAccount savingsAccount : savingsAccounts) {
			if (savingsAccount != null) {
				System.out.println("Account Id : " + savingsAccount.getAccountNumber());
				System.out.println("Account Name : " + savingsAccount.getCustomerName());
				System.out.println("Account Balance : " + savingsAccount.getAccountBalance());
			} else {
				break; // break out of the for loop as soon as all accounts are fetched
			}
		}
	}
}
