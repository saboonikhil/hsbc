package com.hsbc.bank.controller;

import java.util.List;

import com.hsbc.bank.exception.ClientNotFoundException;
import com.hsbc.bank.model.SavingsAccount;
import com.hsbc.bank.service.SavingsAccountService;
import com.hsbc.bank.util.SavingsAccountServiceFactory;

public class SavingsAccountController {

	private SavingsAccountService savingsAccountService = SavingsAccountServiceFactory.getInstance();

	public SavingsAccount openSavingsAccount(String customerName, double accountBalance) {
		SavingsAccount savingsAccount = this.savingsAccountService.createSavingsAccount(customerName, accountBalance);
		return savingsAccount;
	}

	public SavingsAccount openSavingsAccount(String customerName, double accountBalance, String street, String city,
			String state, int zipCode) {
		SavingsAccount savingsAccount = this.savingsAccountService.createSavingsAccount(customerName, accountBalance,
				street, city, state, zipCode);
		return savingsAccount;
	}

	public void deleteSavingsAccount(long accountNumber) throws ClientNotFoundException {
		this.savingsAccountService.deleteSavingsAccount(accountNumber);
	}

	public List<SavingsAccount> fetchSavingsAccounts() {
		List<SavingsAccount> accounts = this.savingsAccountService.fetchSavingsAccounts();
		return accounts;
	}

	public SavingsAccount fetchSavingsAccountByAccountId(long accountNumber) throws ClientNotFoundException {
		SavingsAccount savingsAccount = this.savingsAccountService.fetchSavingsAccountByAccountId(accountNumber);
		return savingsAccount;
	}

	public double withdraw(long accountId, double amount) throws ClientNotFoundException {
		return this.savingsAccountService.withdraw(accountId, amount);
	}

	public double withdrawFromATM(int pin, double amount) throws ClientNotFoundException {
		SavingsAccount savingsAccount = this.savingsAccountService.fetchAccountByPIN(pin);
		return this.savingsAccountService.withdraw(savingsAccount.getAccountNumber(), amount);
	}

	public double deposit(long accountId, double amount) throws ClientNotFoundException {
		return this.savingsAccountService.deposit(accountId, amount);
	}

	public double checkBalance(long accountId) throws ClientNotFoundException {
		return this.savingsAccountService.checkBalance(accountId);
	}

	public void transfer(long senderId, long toId, double amount) throws ClientNotFoundException {
		this.savingsAccountService.transfer(senderId, toId, amount);
	}
}