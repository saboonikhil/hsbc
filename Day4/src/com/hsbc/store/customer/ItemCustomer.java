package com.hsbc.store.customer;

import com.hsbc.store.controller.ItemController;
import com.hsbc.store.model.Item;

public class ItemCustomer {

	public static void main(String[] args) {

		ItemController controller = new ItemController();

		// initializing new accounts
		Item item1 = controller.addItem("Oil", 800, 4);
		Item item2 = controller.addItem("Sugar", 40);
		Item item3 = controller.addItem("Rice", 100, 8);
		
		//updating price and quantity of items
		controller.updatePrice(item1.getItemId(), 1000);
		controller.updateQuantity(item2.getItemId(), 10);
		controller.updatePrice(item3.getItemId(), 85);

		// fetching all accounts stored in the database
		Item[] items = controller.fetchItems();

		for (Item item : items) {
			if (item != null) {
				System.out.println("Item Id : " + item.getItemId());
				System.out.println("Name : " + item.getName());
				System.out.println("Price : " + item.getPrice());
				System.out.println("Quantity : " + item.getQuantity());
			} else {
				break; // break out of the for loop as soon as all accounts are fetched
			}
		}
	}

}
