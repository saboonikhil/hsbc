package com.hsbc.store.model;

public class Item {

	private static long counter = 1000;
	private long itemId;
	private String name;
	private double price;
	private int quantity;

	public Item(String name, double price) {
		this.price = price;
		this.name = name;
		this.itemId = ++counter;
	}

	public Item(String name, double price, int quantity) {
		this.price = price;
		this.name = name;
		this.itemId = ++counter;
		this.quantity = quantity;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getItemId() {
		return itemId;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

}
