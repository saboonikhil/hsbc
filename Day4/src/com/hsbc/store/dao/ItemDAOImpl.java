package com.hsbc.store.dao;

import com.hsbc.store.model.Item;

public class ItemDAOImpl implements ItemDAO {

	private static Item[] items = new Item[100];
	private static int counter;

	@Override
	public Item saveItem(Item item) {
		items[counter++] = item;
		return item;
	}

	@Override
	public Item updateItem(long itemId, Item item) {
		for (int index = 0; index < items.length; index++) {
			if (items[index].getItemId() == itemId) {
				items[index] = item;
				break;
			}
		}
		return item;
	}

	@Override
	public void deleteItem(long itemId) {
		for (int index = 0; index < items.length; index++) {
			if (items[index].getItemId() == itemId) {
				items[index] = null;
				break;
			}
		}
	}

	@Override
	public Item[] fetchItems() {
		return items;
	}

	@Override
	public Item fetchItemByItemId(long itemId) {
		for (int index = 0; index < items.length; index++) {
			if (items[index].getItemId() == itemId) {
				return items[index];
			}
		}
		return null;
	}

}
