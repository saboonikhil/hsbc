package com.hsbc.store.dao;

import com.hsbc.store.model.Item;

public interface ItemDAO {

	Item saveItem(Item item);
	
	Item updateItem(long itemId, Item item);
	
	void deleteItem(long itemId);
	
	Item[] fetchItems();
	
	Item fetchItemByItemId(long itemId);
	
}
