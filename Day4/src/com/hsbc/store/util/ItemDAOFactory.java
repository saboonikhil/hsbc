package com.hsbc.store.util;

import com.hsbc.store.dao.ItemDAO;
import com.hsbc.store.dao.ItemDAOImpl;

public class ItemDAOFactory {

	public static ItemDAO getItemDAO() {
		return new ItemDAOImpl();

	}

}
