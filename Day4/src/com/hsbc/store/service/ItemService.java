package com.hsbc.store.service;

import com.hsbc.store.model.Item;

public interface ItemService {

	public Item createItem(String name, double price);
	
	public Item createItem(String name, double price, int quantity);

	public void deleteItem(long itemId);

	public Item[] fetchItems();

	public Item fetchItemByItemId(long itemId);
	
	public Item updateQuantity(long itemId, int quantity);
	
	public Item updatePrice(long itemId, double price);

}
