package com.hsbc.store.service;

import com.hsbc.store.dao.ItemDAO;
import com.hsbc.store.model.Item;
import com.hsbc.store.util.ItemDAOFactory;

public class ItemServiceImpl implements ItemService {

	private ItemDAO dao = ItemDAOFactory.getItemDAO();

	@Override
	public Item createItem(String name, double price) {
		Item item = new Item(name, price);
		Item itemCreated = this.dao.saveItem(item);
		return itemCreated;
	}

	@Override
	public Item createItem(String name, double price, int quantity) {
		Item item = new Item(name, price, quantity);
		Item itemCreated = this.dao.saveItem(item);
		return itemCreated;
	}
	
	@Override
	public void deleteItem(long itemId) {
		this.dao.deleteItem(itemId);

	}

	@Override
	public Item[] fetchItems() {
		Item[] items = this.dao.fetchItems();
		return items;
	}

	@Override
	public Item fetchItemByItemId(long itemId) {
		Item item = this.dao.fetchItemByItemId(itemId);
		return item;
	}

	@Override
	public Item updateQuantity(long itemId, int quantity) {
		Item item = this.dao.fetchItemByItemId(itemId);
		item.setQuantity(quantity);
		this.dao.updateItem(itemId, item);
		return item;
	}

	@Override
	public Item updatePrice(long itemId, double price) {
		Item item = this.dao.fetchItemByItemId(itemId);
		item.setPrice(price);
		this.dao.updateItem(itemId, item);
		return item;
	}

}
