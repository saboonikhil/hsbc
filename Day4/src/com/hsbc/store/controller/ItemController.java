package com.hsbc.store.controller;

import com.hsbc.store.model.Item;
import com.hsbc.store.service.ItemService;
import com.hsbc.store.util.ItemServiceFactory;

public class ItemController {

	private ItemService itemService = ItemServiceFactory.getInstance();

	public Item addItem(String name, double price) {
		Item item = this.itemService.createItem(name, price);
		return item;
	}
	
	public Item addItem(String name, double price, int quantity) {
		Item item = this.itemService.createItem(name, price, quantity);
		return item;
	}

	public void deleteItem(long itemId) {
		this.itemService.deleteItem(itemId);
	}

	public Item[] fetchItems() {
		Item[] items = this.itemService.fetchItems();
		return items;
	}

	public Item fetchItemByItemId(long itemId) {
		Item item = this.itemService.fetchItemByItemId(itemId);
		return item;
	}

	public Item updateQuantity(long itemId, int quantity) {
		Item item = this.itemService.updateQuantity(itemId, quantity);
		return item;
	}
	
	public Item updatePrice(long itemId, double price) {
		Item item = this.itemService.updatePrice(itemId, price);
		return item;
	}

}
