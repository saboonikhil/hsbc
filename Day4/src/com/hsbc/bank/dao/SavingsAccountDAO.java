package com.hsbc.bank.dao;

import com.hsbc.bank.model.SavingsAccount;

public interface SavingsAccountDAO {
	
	
	SavingsAccount saveSavingsAccount(SavingsAccount savingsAccount);
	
	SavingsAccount updateSavingsAccount(long accountNumber, SavingsAccount savingsAccount);
	
	void deleteSavingsAccount(long accountNumber);
	
	SavingsAccount[] fetchSavingsAccounts();
	
	SavingsAccount fetchSavingsAccountByAccountId(long accountNumber);

}