package com.hsbc.bank.client;

import com.hsbc.bank.controller.SavingsAccountController;
import com.hsbc.bank.model.SavingsAccount;

public class SavingsAccountClient {

	public static void main(String[] args) {

		SavingsAccountController controller = new SavingsAccountController();

		//initializing new accounts
		SavingsAccount account1 = controller.openSavingsAccount("Rakesh", 50000);
		SavingsAccount account2 = controller.openSavingsAccount("Rajesh", 20000);
		SavingsAccount account3 = controller.openSavingsAccount("Ramesh", 10000, "N.S. Road", "Kolkata", "WB", 700001);

		//transferring 20000 from account1 to account2
		controller.transfer(account1.getAccountNumber(), account2.getAccountNumber(), 20000);
		
		//transferring 10000 from account2 to account3
		controller.transfer(account2.getAccountNumber(), account3.getAccountNumber(), 10000);

		//depositing 10000 to account1
		double newAccBalance = controller.deposit(account1.getAccountNumber(), 10000);

		//fetching all accounts stored in the database
		SavingsAccount[] savingsAccounts = controller.fetchSavingsAccounts();

		for (SavingsAccount savingsAccount : savingsAccounts) {
			if (savingsAccount != null) {
				System.out.println("Account Id : " + savingsAccount.getAccountNumber());
				System.out.println("Account Name : " + savingsAccount.getCustomerName());
				System.out.println("Account Balance : " + savingsAccount.getAccountBalance());
			} else {
				break; //break out of the for loop as soon as all accounts are fetched
			}
		}
	}
}
