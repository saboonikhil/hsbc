package com.hsbc.bank.service;

import com.hsbc.bank.model.SavingsAccount;

public interface SavingsAccountService {

	public SavingsAccount createSavingsAccount(String customerName, double accountBalance);
	
	public SavingsAccount createSavingsAccount(String customerName, double accountBalance, String street, String city, String state, int zipCode);

	public void deleteSavingsAccount(long accountNumber);

	public SavingsAccount[] fetchSavingsAccounts();

	public SavingsAccount fetchAccountByPIN(int pin);

	public SavingsAccount fetchSavingsAccountByAccountId(long accountNumber);

	public double withdraw(long accountId, double amount);

	public double deposit(long accountId, double amount);

	public double checkBalance(long accountId);

	public void transfer(long accountId, long toId, double amount);
}