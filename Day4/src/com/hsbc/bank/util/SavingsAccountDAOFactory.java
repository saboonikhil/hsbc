package com.hsbc.bank.util;

import com.hsbc.bank.dao.ArrayBackedSavingsAccountDAOImpl;
import com.hsbc.bank.dao.ListBackedSavingsAccountDAOImpl;
import com.hsbc.bank.dao.SavingsAccountDAO;

public class SavingsAccountDAOFactory {

	public static SavingsAccountDAO getSavingsAccountDAO(int value) {
		if (value == 1) {
			return new ArrayBackedSavingsAccountDAOImpl();
		} else {
			return new ListBackedSavingsAccountDAOImpl();
		}
	}

}
