class BillAmountTwo {
  public static void main(String[] args) {
    double amount = Double.parseDouble(args[0]);
    String code = args[1];
    code = code.toUpperCase();

    if(code.equals("TN"))
      amount += 0.18*amount;
    else if(code.equals("MH"))
      amount += 0.20*amount;
    else if(code.equals("KA"))
      amount += 0.15*amount;
    else
      amount += 0.12*amount; 
    
    System.out.println("Total bill amount after tax is: " + amount);
  }
}