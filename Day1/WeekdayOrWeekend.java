import java.util.Scanner;

class WeekdayOrWeekend {
  public static void main(String[] args) {
    String day;
    System.out.print("Enter the day: ");

    Scanner sc = new Scanner(System.in);
    day = sc.nextLine();
    day = day.toLowerCase();
    
    if(day.equals("sunday") || day.equals("saturday"))
      System.out.println(day + " is a weekend");
    else
      System.out.println(day + " is a weekday");
  }
}