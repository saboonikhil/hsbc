import java.util.Scanner;

class VowelOrConsonant {
  public static void main(String[] args) {
    char ch;
    System.out.print("Enter the character: ");

    Scanner sc = new Scanner(System.in);
    ch = sc.next().charAt(0);

    if(ch == 'a' || ch == 'e' || ch == 'i' || ch == 'o' || ch == 'u' )
      System.out.println(ch + " is vowel");
    else
      System.out.println(ch + " is consonant");
  }
}