import java.util.Scanner;
import java.util.Arrays;

class MinAndMax {
  public static void main(String[] args) {
    int temp;
    int[] arr =  new int[5];
     
    for(int i=0;i<5;i++){
      arr[i] = Integer.parseInt(args[i]);
    }

    for (int i = 0; i < arr.length; i++) {     
      for (int j = i+1; j < arr.length; j++) {     
         if(arr[i] > arr[j]) {    
             temp = arr[i];    
             arr[i] = arr[j];    
             arr[j] = temp;    
         }     
      }     
    }    

    System.out.println("Maximum value is " + arr[0]);
    System.out.println("Minimum value is " + arr[4]);
  }
}