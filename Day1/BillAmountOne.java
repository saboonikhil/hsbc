import java.util.Scanner;

class BillAmountOne {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    System.out.print("Enter the bill amount: ");
    double amount = sc.nextDouble();

    amount += 0.15*amount;
    System.out.println("Total bill amount after tax is: " + amount);
  }
}