package com.hsbc.bank.util;

import com.hsbc.bank.service.SavingsAccountService;
import com.hsbc.bank.service.SavingsAccountServiceImpl;

public class SavingsAccountServiceFactory {

	public static SavingsAccountService getInstance() {
		return new SavingsAccountServiceImpl();
	}

}
