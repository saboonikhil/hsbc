package com.hsbc.bank.client;

import java.util.Collection;

import com.hsbc.bank.controller.SavingsAccountController;
import com.hsbc.bank.model.SavingsAccount;

public class SavingsAccountClient {

	public static void main(String[] args) {

		SavingsAccountController controller = new SavingsAccountController();

		// initializing new accounts
		SavingsAccount account1 = controller.openSavingsAccount("Rakesh", 50000);
		SavingsAccount account2 = controller.openSavingsAccount("Rajesh", 20000);
		SavingsAccount account3 = controller.openSavingsAccount("Ramesh", 10000, "N.S. Road", "Kolkata", "WB", 700001);

		// fetching all accounts stored in the database
		Collection<SavingsAccount> savingsAccounts = controller.fetchSavingsAccounts();
		for (SavingsAccount savingsAccount : savingsAccounts) {
			System.out.println("Account Id : " + savingsAccount.getAccountNumber());
			System.out.println("Account Name : " + savingsAccount.getCustomerName());
			System.out.println("Account Balance : " + savingsAccount.getAccountBalance());
		}
	}
}
