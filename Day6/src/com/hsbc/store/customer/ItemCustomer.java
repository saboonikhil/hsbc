package com.hsbc.store.customer;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.List;

import com.hsbc.store.controller.ItemController;
import com.hsbc.store.model.Item;

public class ItemCustomer {

	@SuppressWarnings("unchecked")
	public static void main(String[] args) {

		ItemController controller = new ItemController();

		controller.addItem("Oil", 400);
		controller.addItem("Sugar", 50);
		controller.addItem("Atta", 40);

		// fetching all items stored in the database
		List<Item> items = controller.fetchItems();
		for (Item item : items) {
			System.out.println("Item Id : " + item.getItemId());
			System.out.println("Name : " + item.getName());
			System.out.println("Price : " + item.getPrice());
		}

		System.out.println("===================================================");
		System.out.println("After Serialization");

		FileInputStream fis = null;
		ObjectInputStream ois = null;
		try {
			fis = new FileInputStream("output.txt");
			ois = new ObjectInputStream(fis);
			List<Item> list = (ArrayList<Item>) ois.readObject();
			for (Item item : list) {
				System.out.println(item.getName() + " : " + item.getItemId());
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (ois != null) {
				try {
					ois.close();
				} catch (IOException ex) {
					ex.printStackTrace();
				}
			}
		}
	}

}
