package com.hsbc.store.dao;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import com.hsbc.store.model.Item;

public class ItemDAOImpl implements ItemDAO {

	private static List<Item> items = new ArrayList<>();

	@Override
	public Item saveItem(Item item) {
		items.add(item);
		try {
			FileOutputStream fos = new FileOutputStream("output.txt");
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(items);
			oos.close();
			fos.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return item;

	}

	@Override
	public Item updateItem(long itemId, Item item) {
		for (Item i : items) {
			if (i.getItemId() == itemId) {
				i = item;
			}
		}
		return item;
	}

	@Override
	public void deleteItem(long itemId) {
		for (Item item : items) {
			if (item.getItemId() == itemId) {
				items.remove(item);
			}
		}

	}

	@Override
	public List<Item> fetchItems() {
		return items;
	}

	@Override
	public Item fetchItemByItemId(long itemId) {
		for (Item item : items) {
			if (item.getItemId() == itemId) {
				return item;
			}
		}
		return null;
	}

}
