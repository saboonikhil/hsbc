package com.hsbc.store.dao;

import java.util.List;

import com.hsbc.store.model.Item;

public interface ItemDAO {

	Item saveItem(Item item);

	Item updateItem(long itemId, Item item);

	void deleteItem(long itemId);

	List<Item> fetchItems();

	Item fetchItemByItemId(long itemId);

}
