package com.hsbc.store.util;

import com.hsbc.store.service.ItemService;
import com.hsbc.store.service.ItemServiceImpl;

public class ItemServiceFactory {

	public static ItemService getInstance() {
		return new ItemServiceImpl();
	}
}