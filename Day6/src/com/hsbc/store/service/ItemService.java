package com.hsbc.store.service;

import java.util.List;

import com.hsbc.store.model.Item;

public interface ItemService {

	public Item createItem(String name, double price);

	public void deleteItem(long itemId);

	public List<Item> fetchItems();

	public Item fetchItemByItemId(long itemId);

	public Item updatePrice(long itemId, double price);

}
