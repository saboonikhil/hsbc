package com.hsbc.store.model;

import java.io.Serializable;

public class Item implements Serializable {

	private static long counter = 1000;
	private long itemId;
	private String name;
	private double price;

	public Item(String name, double price) {
		this.price = price;
		this.name = name;
		this.itemId = ++counter;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getItemId() {
		return itemId;
	}

}
