package com.hsbc.ems.util;

import com.hsbc.ems.dao.EmployeeDAO;
import com.hsbc.ems.dao.EmployeeDAOImpl;

public class EmployeeDAOFactory {

	public static EmployeeDAO getEmployeeDAO() {
		return new EmployeeDAOImpl();

	}

}
