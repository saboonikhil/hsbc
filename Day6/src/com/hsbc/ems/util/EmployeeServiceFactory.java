package com.hsbc.ems.util;

import com.hsbc.ems.service.EmployeeService;
import com.hsbc.ems.service.EmployeeServiceImpl;

public class EmployeeServiceFactory {

	public static EmployeeService getInstance() {
		return new EmployeeServiceImpl();
	}
}