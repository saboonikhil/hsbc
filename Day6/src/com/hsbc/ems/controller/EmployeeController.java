package com.hsbc.ems.controller;

import java.util.Set;

import com.hsbc.ems.exception.InsufficientLeaveBalanceException;
import com.hsbc.ems.model.Employee;
import com.hsbc.ems.service.EmployeeService;
import com.hsbc.ems.util.EmployeeServiceFactory;

public class EmployeeController {

	private EmployeeService employeeService = EmployeeServiceFactory.getInstance();

	public Employee addEmployee(String name, int age) {
		Employee employee = this.employeeService.createEmployee(name, age);
		return employee;
	}

	public Employee addEmployee(String name, int age, double salary) {
		Employee employee = this.employeeService.createEmployee(name, age, salary);
		return employee;
	}

	public void deleteEmployee(long employeeId) {
		this.employeeService.deleteEmployee(employeeId);
	}

	public Set<Employee> fetchEmployees() {
		Set<Employee> employees = this.employeeService.fetchEmployees();
		return employees;
	}

	public Employee fetchEmployeeByEmployeeId(long employeeId) {
		Employee employee = this.employeeService.fetchEmployeeByEmployeeId(employeeId);
		return employee;
	}

	public void applyLeave(long employeeId, int numberOfDays) throws InsufficientLeaveBalanceException {
		this.employeeService.applyLeave(employeeId, numberOfDays);
	}

}
