package com.hsbc.ems.service;

import java.util.Set;

import com.hsbc.ems.exception.InsufficientLeaveBalanceException;
import com.hsbc.ems.model.Employee;

public interface EmployeeService {

	public Employee createEmployee(String name, int age);
	
	public Employee createEmployee(String name, int age, double salary);

	public void deleteEmployee(long employeeId);

	public Set<Employee> fetchEmployees();

	public Employee fetchEmployeeByEmployeeId(long employeeId);
	
	public Employee fetchEmployeeByName(String name);
	
	public void applyLeave(long employeeId, int numberOfDays) throws InsufficientLeaveBalanceException;

}
