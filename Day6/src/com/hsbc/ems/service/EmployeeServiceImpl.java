package com.hsbc.ems.service;

import java.util.Set;

import com.hsbc.ems.dao.EmployeeDAO;
import com.hsbc.ems.exception.InsufficientLeaveBalanceException;
import com.hsbc.ems.model.Employee;
import com.hsbc.ems.util.EmployeeDAOFactory;

public class EmployeeServiceImpl implements EmployeeService {

	private EmployeeDAO dao = EmployeeDAOFactory.getEmployeeDAO();

	@Override
	public void deleteEmployee(long employeeId) {
		this.dao.deleteEmployee(employeeId);

	}

	@Override
	public Employee fetchEmployeeByEmployeeId(long employeeId) {
		Employee employee = this.dao.fetchEmployeeByEmployeeId(employeeId);
		return employee;
	}

	@Override
	public Employee createEmployee(String name, int age) {
		Employee employee = new Employee(name, age);
		Employee employeeCreated = this.dao.saveEmployee(employee);
		return employeeCreated;
	}

	@Override
	public Employee createEmployee(String name, int age, double salary) {
		Employee employee = new Employee(name, age, salary);
		Employee employeeCreated = this.dao.saveEmployee(employee);
		return employeeCreated;
	}

	@Override
	public Set<Employee> fetchEmployees() {
		Set<Employee> employees = this.dao.fetchEmployees();
		return employees;
	}

	@Override
	public Employee fetchEmployeeByName(String name) {
		Employee employee = this.dao.fetchEmployeeByName(name);
		return employee;
	}

	@Override
	public void applyLeave(long employeeId, int numberOfDays) throws InsufficientLeaveBalanceException {
		Employee employee = this.dao.fetchEmployeeByEmployeeId(employeeId);
		if (numberOfDays > 10) {
			System.out.println("Hi " + employee.getName() + "! You can apply leave for maximum 10 days");
			return;
		}
		if (employee.getLeaveBalance() >= numberOfDays) {
			employee.setLeaveBalance((employee.getLeaveBalance() - numberOfDays));
			this.dao.updateEmployee(employeeId, employee);
		} else {
			throw new InsufficientLeaveBalanceException("Hi " + employee.getName()
					+ "! Your applied leave exceeds your balance " + employee.getLeaveBalance());
		}
	}

}
