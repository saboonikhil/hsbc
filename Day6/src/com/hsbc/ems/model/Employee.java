package com.hsbc.ems.model;

public class Employee implements Comparable<Employee> {

	private static long counter = 1000;
	private long employeeId;
	private String name;
	private int age;
	private double salary;
	private int leaveBalance;

	public Employee(String name, int age, double salary) {
		this.salary = salary;
		this.name = name;
		this.employeeId = counter++;
		this.age = age;
		this.leaveBalance = 40;
	}

	public Employee(String name, int age) {
		this.name = name;
		this.employeeId = counter++;
		this.age = age;
		this.leaveBalance = 40;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getEmployeeId() {
		return employeeId;
	}

	public int getLeaveBalance() {
		return leaveBalance;
	}

	public void setLeaveBalance(int leaveBalance) {
		this.leaveBalance = leaveBalance;
	}

	@Override
	public String toString() {
		return "Employee [ID=" + employeeId + ", Name=" + name + ", Age=" + age + ", Leave Balance=" + leaveBalance + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (employeeId ^ (employeeId >>> 32));
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Employee))
			return false;
		Employee other = (Employee) obj;
		if (employeeId != other.employeeId)
			return false;
		return true;
	}

	@Override
	public int compareTo(Employee e) {
		return (int) (this.employeeId - e.employeeId);
	}

}
