package com.hsbc.ems.dao;

import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

import com.hsbc.ems.model.Employee;

public class EmployeeDAOImpl implements EmployeeDAO {

	private static Set<Employee> employees = new TreeSet<Employee>();

	@Override
	public Employee saveEmployee(Employee employee) {
		employees.add(employee);
		return employee;
	}

	@Override
	public Employee updateEmployee(long employeeId, Employee employee) {
		Iterator<Employee> it = employees.iterator();
		while (it.hasNext()) {
			Employee employeeFetched = it.next();
			if (employeeFetched.getEmployeeId() == employeeId) {
				employeeFetched = employee;
			}
		}
		return employee;
	}

	@Override
	public void deleteEmployee(long employeeId) {
		Iterator<Employee> it = employees.iterator();
		while (it.hasNext()) {
			if (it.next().getEmployeeId() == employeeId) {
				employees.remove(it.next());
			}
		}
	}

	@Override
	public Set<Employee> fetchEmployees() {
		return employees;
	}

	@Override
	public Employee fetchEmployeeByEmployeeId(long employeeId) {
		Iterator<Employee> it = employees.iterator();
		while (it.hasNext()) {
			Employee employeeFetched = it.next();
			if (employeeFetched.getEmployeeId() == employeeId) {
				return employeeFetched;
			}
		}
		return null;
	}

	@Override
	public Employee fetchEmployeeByName(String name) {
		Iterator<Employee> it = employees.iterator();
		while (it.hasNext()) {
			Employee employeeFetched = it.next();
			if (employeeFetched.getName().equals(name)) {
				return employeeFetched;
			}
		}
		return null;
	}

}
