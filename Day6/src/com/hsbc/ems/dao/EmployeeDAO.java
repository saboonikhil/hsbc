package com.hsbc.ems.dao;

import java.util.Set;

import com.hsbc.ems.model.Employee;

public interface EmployeeDAO {

	Employee saveEmployee(Employee employee);
	
	Employee updateEmployee(long employeeId, Employee employee);
	
	void deleteEmployee(long employeeId);
	
	Set<Employee> fetchEmployees();
	
	Employee fetchEmployeeByEmployeeId(long employeeId);
	
	Employee fetchEmployeeByName(String name);
	
}
