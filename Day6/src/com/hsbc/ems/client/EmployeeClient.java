package com.hsbc.ems.client;

import java.util.Iterator;
import java.util.Set;

import com.hsbc.bank.model.SavingsAccount;
import com.hsbc.ems.controller.EmployeeController;
import com.hsbc.ems.exception.InsufficientLeaveBalanceException;
import com.hsbc.ems.model.Employee;

public class EmployeeClient {

	public static void main(String[] args) {

		EmployeeController controller = new EmployeeController();

		// initializing new employees
		Employee employee1 = controller.addEmployee("Rakesh", 40);
		Employee employee2 = controller.addEmployee("Rajesh", 45);
		Employee employee3 = controller.addEmployee("Ramesh", 50);

		// fetching all employees stored in the database
		Set<Employee> employees = controller.fetchEmployees();
		Iterator<Employee> it = employees.iterator();
		while (it.hasNext()) {
			System.out.println(it.next());
		}
		
		System.out.println("===================================================");

		try {
			controller.applyLeave(employee2.getEmployeeId(), 10);
			controller.applyLeave(employee1.getEmployeeId(), 40);
			controller.applyLeave(employee2.getEmployeeId(), 10);
			controller.applyLeave(employee2.getEmployeeId(), 10);
			controller.applyLeave(employee2.getEmployeeId(), 10);
			controller.applyLeave(employee1.getEmployeeId(), 4);
			controller.applyLeave(employee3.getEmployeeId(), 9);
			controller.applyLeave(employee2.getEmployeeId(), 4);
		} catch (InsufficientLeaveBalanceException e) {
			System.out.println(e.getMessage());
		}
		
		System.out.println("===================================================");
		
		Set<Employee> employees2 = controller.fetchEmployees();
		Iterator<Employee> it1 = employees2.iterator();
		while (it1.hasNext()) {
			System.out.println(it1.next());
		}
	}

}
