package com.hsbc.ems.exception;

public class InsufficientLeaveBalanceException extends Exception {
	
	public InsufficientLeaveBalanceException(String message) {
		super(message);
	}

	@Override
	public String getMessage() {
		return super.getMessage();
	}

}
