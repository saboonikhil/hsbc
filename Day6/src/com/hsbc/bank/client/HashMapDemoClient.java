package com.hsbc.bank.client;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import com.hsbc.bank.model.SavingsAccount;

public class HashMapDemoClient {

	public static void main(String[] args) {

		Map<SavingsAccount, Long> mapOfSavingsAccount = new TreeMap<>();
		SavingsAccount deepa = new SavingsAccount("Deepa", 45_000);
		SavingsAccount veena = new SavingsAccount("Veena", 55_000);
		SavingsAccount harsha = new SavingsAccount("Harsha", 65_000);
		SavingsAccount vinay = new SavingsAccount("Vinay", 85_000);
		SavingsAccount kuldeep = new SavingsAccount("Kuldeep", 1_05_000);

		mapOfSavingsAccount.put(deepa, deepa.getAccountNumber());
		mapOfSavingsAccount.put(veena, veena.getAccountNumber());
		mapOfSavingsAccount.put(harsha, harsha.getAccountNumber());
		mapOfSavingsAccount.put(vinay, vinay.getAccountNumber());
		mapOfSavingsAccount.put(kuldeep, kuldeep.getAccountNumber());

		Set<SavingsAccount> setOfKeys = mapOfSavingsAccount.keySet();

		Iterator<SavingsAccount> keys = setOfKeys.iterator();

		while (keys.hasNext()) {
			SavingsAccount key = keys.next();
		}

		Collection<Long> accountNumbers = mapOfSavingsAccount.values();

		Iterator<Long> its = accountNumbers.iterator();

		Set<Map.Entry<SavingsAccount, Long>> setOfEntries = mapOfSavingsAccount.entrySet();

		Iterator<Map.Entry<SavingsAccount, Long>> iter = setOfEntries.iterator();

		while (iter.hasNext()) {
			Map.Entry<SavingsAccount, Long> entry = iter.next();
			System.out.println(entry.getKey().getCustomerName() + " with Account Number: " + entry.getValue());
		}
	}
}
