package com.hsbc.store.dao;

import java.util.List;

import com.hsbc.store.model.Item;

public interface ItemDAO {

	Item saveItem(Item item);

	List<Item> fetchItems();

	Item fetchItemByItemId(long itemId);

	void deleteItem(String name);

	static ItemDAO getInstance() {
		return new ItemDAOImpl();
	}

}
