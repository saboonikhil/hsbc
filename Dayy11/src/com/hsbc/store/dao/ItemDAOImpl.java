package com.hsbc.store.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.hsbc.store.model.Item;

public class ItemDAOImpl implements ItemDAO {

	private static final String SELECT_QUERY = "select * from items";
	private static final String INSERT_QUERY = "insert into items (name, price) values(?, ?)";
	private static final String DELETE_QUERY = "delete from items where name = ?";
	private static final String SELECT_ITEM_QUERY = "select * from items where itemId = ?";

	static Connection getConnection() {
		try {
			Class.forName("org.apache.derby.jdbc.ClientDriver");
			Connection connection = DriverManager.getConnection("jdbc:derby://localhost:1527/mydb", "app", "password");
			return connection;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public Item saveItem(Item item) {
		Connection connection = getConnection();
		try {
			PreparedStatement preparedStatement = connection.prepareStatement(INSERT_QUERY);
			preparedStatement.setString(1, item.getName());
			preparedStatement.setDouble(2, item.getPrice());
			int rowsUpdated = preparedStatement.executeUpdate();
			if (rowsUpdated > 0)
				return item;
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("Item not inserted");
		return item;
	}

	@Override
	public List<Item> fetchItems() {
		List<Item> items = new ArrayList<>();
		Connection connection = getConnection();

		try {
			Statement statement = connection.createStatement();
			ResultSet rs = statement.executeQuery(SELECT_QUERY);
			while (rs.next()) {
				items.add(new Item(rs.getLong("itemId"), rs.getString("name"), rs.getDouble("price")));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return items;
	}

	public void deleteItem(String name) {
		Connection connection = getConnection();
		try {
			PreparedStatement preparedStatement = connection.prepareStatement(DELETE_QUERY);
			preparedStatement.setString(1, name);
			int rowsUpdated = preparedStatement.executeUpdate();
			if (rowsUpdated > 0)
				return;
		} catch (SQLException e) {
			e.printStackTrace();
		}

		System.out.println("Item Not Deleted.");
	}

	@Override
	public Item fetchItemByItemId(long itemId) {
		Connection connection = getConnection();
		try {
			PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ITEM_QUERY);
			preparedStatement.setLong(1, itemId);
			ResultSet rs = preparedStatement.executeQuery();
			while (rs.next()) {
				Item item = new Item(rs.getLong("itemId"), rs.getString("name"), rs.getDouble("price"));
				return item;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

}
