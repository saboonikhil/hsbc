package com.hsbc.store.service;

import java.util.List;

import com.hsbc.store.dao.ItemDAO;
import com.hsbc.store.model.Item;

public class ItemServiceImpl implements ItemService {

	private ItemDAO itemDAO = ItemDAO.getInstance();

	@Override
	public Item createItem(Item item) {
		return this.itemDAO.saveItem(item);
	}

	@Override
	public List<Item> fetchItems() {
		return this.itemDAO.fetchItems();
	}

	@Override
	public void deleteItem(String itemName) {
		this.itemDAO.deleteItem(itemName);

	}

	@Override
	public Item fetchItemByItemId(long itemId) {
		return itemDAO.fetchItemByItemId(itemId);
	}

}
