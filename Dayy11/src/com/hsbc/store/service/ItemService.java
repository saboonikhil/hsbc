package com.hsbc.store.service;

import java.util.List;

import com.hsbc.store.model.Item;

public interface ItemService {

	static final ItemService itemService = new ItemServiceImpl();

	Item createItem(Item item);

	List<Item> fetchItems();

	Item fetchItemByItemId(long itemId);

	void deleteItem(String itemName);

	static ItemService getItemService() {
		return new ItemServiceImpl();
	}

}
