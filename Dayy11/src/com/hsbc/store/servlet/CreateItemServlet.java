package com.hsbc.store.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hsbc.store.model.Item;
import com.hsbc.store.service.ItemService;

public class CreateItemServlet extends HttpServlet {

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String name = request.getParameter("name");
		double price = Double.parseDouble(request.getParameter("price"));

		Item item = new Item(name, price);
		ItemService itemService = ItemService.getItemService();
		itemService.createItem(item);
		response.sendRedirect("listItems");
	}

}
