package com.hsbc.store.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hsbc.store.service.ItemService;

public class DeleteItemServlet extends HttpServlet {

	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {

		String itemName = req.getParameter("name");
		ItemService itemService = ItemService.getItemService();
		itemService.deleteItem(itemName);
		res.sendRedirect("listItems");

	}

}
