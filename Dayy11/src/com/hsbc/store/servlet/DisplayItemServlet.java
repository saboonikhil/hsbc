package com.hsbc.store.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hsbc.store.model.Item;
import com.hsbc.store.service.ItemService;

public class DisplayItemServlet extends HttpServlet {

	public void doGet(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException, NullPointerException {

		long itemId = Long.parseLong(req.getParameter("itemId"));
		ItemService itemService = ItemService.getItemService();

		Item item = itemService.fetchItemByItemId(itemId);

		PrintWriter pw = res.getWriter();
		pw.write("<h1>Requested Item : </h1><br>");

		pw.write("<table>");
		pw.write("<tr>");
		pw.write("<th>Item ID</th><th>Name</th><th>Price</th>");
		pw.write("</tr>");
		pw.write("<td>" + item.getItemId() + "</td>");
		pw.write("<td>" + item.getName() + "</td>");
		pw.write("<td>" + item.getPrice() + "</td>");
		pw.write("</table>");
	}

}
