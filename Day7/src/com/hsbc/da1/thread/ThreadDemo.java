package com.hsbc.da1.thread;

public class ThreadDemo extends Thread {

	public ThreadDemo(String name) {
		super(name);
	}

	public static void main(String[] args) {

		ThreadDemo t = new ThreadDemo("Child Thread");
		t.start();

		Runnable threadGooglePhotos = new GooglePhotos();
		Runnable threadFlickr = new Flickr();
		Runnable threadPicassa = new Picassa();

		Thread t1 = new Thread(threadGooglePhotos, "GooglePhotos Thread");
		Thread t2 = new Thread(threadFlickr, "Flickr Thread");
		Thread t3 = new Thread(threadPicassa, "Picassa Thread");

		t1.start();
		t2.start();
		t3.start();

		try {
			t1.join();
			t2.join();
			t3.join();
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}

		try {
			t.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("Exiting " + Thread.currentThread().getName());

	}

	public void run() {
		System.out.println("Entering " + Thread.currentThread().getName());
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			System.out.println(e.getMessage());
		}
		System.out.println("Exiting " + Thread.currentThread().getName());
	}

}

class GooglePhotos implements Runnable {

	@Override
	public void run() {
		System.out.println("Entering " + Thread.currentThread().getName());
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("Exiting " + Thread.currentThread().getName());
	}

}

class Flickr implements Runnable {

	@Override
	public void run() {
		System.out.println("Entering " + Thread.currentThread().getName());
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("Exiting " + Thread.currentThread().getName());
	}

}

class Picassa implements Runnable {

	@Override
	public void run() {
		System.out.println("Entering " + Thread.currentThread().getName());
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("Exiting " + Thread.currentThread().getName());
	}

}
