package com.hsbc.da1.bockingqueue;

public class Message {

	private String command;

	public Message(String command) {
		this.command = command;
	}

	public String getCommand() {
		return this.command;
	}

}
